package com.ss.mpp;

import java.io.File;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.ss.mpp.util.FileUtil;

public class MainActivity extends Activity {

	File[] currentMeetingSlides;
	int currentSlideNumber;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		setTitle(R.string.title_activity_main);
		
		FileUtil.createRootAppFolder(MPPContants.ZIP_NAME);
		
		Button createPresentation = (Button) findViewById(R.id.create);
		createPresentation.setOnClickListener(onCreatePresentation);
		
		Button startPresentation = (Button) findViewById(R.id.start);
		startPresentation.setOnClickListener(onStartPresentation);

		Button joinPresentation = (Button) findViewById(R.id.join);
		joinPresentation.setOnClickListener(onJoinPresentation);
	}

	private View.OnClickListener onCreatePresentation = new View.OnClickListener() {
		public void onClick(View v) {
			Intent i = new Intent(MainActivity.this, CreatePresentationActivity.class);
			startActivity(i);
		}
	};

	private View.OnClickListener onStartPresentation = new View.OnClickListener() {
		public void onClick(View v) {
			Intent i = new Intent(MainActivity.this, StartPresentationActivity.class);
			startActivity(i);
		}
	};

	private View.OnClickListener onJoinPresentation = new View.OnClickListener() {
		public void onClick(View v) {
			Intent i = new Intent(MainActivity.this, JoinPresentationActivity.class);
			startActivity(i);
		}
	};
}
