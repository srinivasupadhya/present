package com.ss.mpp.util;

import java.io.File;
import java.util.Arrays;
import java.util.Comparator;

public class CollectionUtil {
	public static void sortSlides(File[] files) {
		Arrays.sort(files, new Comparator<Object>() {
			public int compare(Object f1, Object f2) {
				String f1Name = ((File) f1).getName();
				Integer f1Number = Integer.parseInt(f1Name.substring(0, f1Name.lastIndexOf(".")));
				String f2Name = ((File) f2).getName();
				Integer f2Number = Integer.parseInt(f2Name.substring(0, f2Name.lastIndexOf(".")));
				return f1Number.compareTo(f2Number);
			}
		});
	}

	public static boolean isEmpty(Object[] objs) {
		if (objs == null || objs.length == 0)
			return true;
		return false;
	}
}
