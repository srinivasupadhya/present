package com.ss.mpp.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.FileUtils;

import android.os.Environment;

public class FileUtil {
	// /mnt/sdcard/Android/data/com.tw.present/files/mypresentation
	// Environment.getExternalStorageDirectory()
	private static final String ROOT_APP_FOLDER_ABSOLUTE_PATH = "/mnt/sdcard/Present";
	private static final String APP_TMP_FOLDER_ABSOLUTE_PATH = ROOT_APP_FOLDER_ABSOLUTE_PATH + "/tmp";
	private static final String USER_PRESENTATIONS_FOLDER_ABSOLUTE_PATH = ROOT_APP_FOLDER_ABSOLUTE_PATH + "/MyPresentations";
	private static final String OTHER_PRESENTATIONS_FOLDER_ABSOLUTE_PATH = ROOT_APP_FOLDER_ABSOLUTE_PATH + "/OtherPresentations";
	private static final int BUFFER = 2048;

	public static void createRootAppFolder(String presentationName) {
		File rootAppFolder = new File(ROOT_APP_FOLDER_ABSOLUTE_PATH);
		if (!rootAppFolder.exists())
			rootAppFolder.mkdir();

		File userPresentationsFolder = new File(USER_PRESENTATIONS_FOLDER_ABSOLUTE_PATH);
		if (!userPresentationsFolder.exists())
			userPresentationsFolder.mkdir();

		File otherPresentationsFolder = new File(OTHER_PRESENTATIONS_FOLDER_ABSOLUTE_PATH);
		if (!otherPresentationsFolder.exists())
			otherPresentationsFolder.mkdir();
		else
			cleanDirectory(otherPresentationsFolder);

		File appTmpFolder = new File(APP_TMP_FOLDER_ABSOLUTE_PATH);
		if (!appTmpFolder.exists())
			appTmpFolder.mkdir();
		else
			cleanDirectory(appTmpFolder);
	}

	public static File createPresentationDirectory(String presentationName, List<String> filePaths) {
		File presentationFolder = getUserPresentationDirectory(presentationName);
		createOrCleanDirectory(presentationFolder);

		int i = 0;
		for (String currentFilePath : filePaths) {
			String currentFileName = currentFilePath.substring(currentFilePath.lastIndexOf("/") + 1);
			String extension = currentFileName.substring(currentFileName.lastIndexOf(".") + 1);
			String destFilePath = presentationFolder.getAbsolutePath() + "/" + ++i + "." + extension;

			copyfile(currentFilePath, destFilePath);
		}

		String zipFilePath = APP_TMP_FOLDER_ABSOLUTE_PATH + "/" + presentationName + ".zip";
		File zipFile = new File(zipFilePath);

		zip(presentationFolder.listFiles(), zipFile);

		CommunicationUtil.uploadFile(zipFile.getAbsolutePath());

		zipFile.delete();

		return presentationFolder;
	}

	public static String[] getUserPresentations() {
		File userPresentationFolder = new File(USER_PRESENTATIONS_FOLDER_ABSOLUTE_PATH);
		File[] filesInDir = userPresentationFolder.listFiles();
		if (filesInDir != null && filesInDir.length > 0) {
			List<String> presentationList = new ArrayList<String>();
			for (int i = 0; i < filesInDir.length; i++) {
				if (filesInDir[i].isDirectory())
					presentationList.add(filesInDir[i].getName());
			}
			Collections.sort(presentationList);
			String[] presentationArray = new String[presentationList.size()];
			presentationList.toArray(presentationArray);
			return presentationArray;
		}
		return new String[0];
	}

	public static File[] getUserPresentationSlides(String presentationName) {
		File presentationFolder = FileUtil.getUserPresentationDirectory(presentationName);
		return presentationFolder.listFiles(new SlideFileNameFilter());
	}

	public static File getUserPresentationDirectory(String presentationName) {
		String presentationFolderAbsolutePath = USER_PRESENTATIONS_FOLDER_ABSOLUTE_PATH + "/" + presentationName;
		File presentationFolder = new File(presentationFolderAbsolutePath);
		return presentationFolder;
	}

	public static File[] getOtherPresentationSlides(String meetingId) {
		File presentationFolder = FileUtil.getOtherPresentationDirectory(meetingId);
		return presentationFolder.listFiles(new SlideFileNameFilter());
	}

	public static File getOtherPresentationDirectory(String meetingId) {
		String zipFilePath = APP_TMP_FOLDER_ABSOLUTE_PATH + "/" + meetingId + ".zip";

		CommunicationUtil.downloadFile(meetingId, zipFilePath);

		String presentationFolderAbsolutePath = OTHER_PRESENTATIONS_FOLDER_ABSOLUTE_PATH + "/" + meetingId + "/";

		File presentationFolder = new File(presentationFolderAbsolutePath);
		createOrCleanDirectory(presentationFolder);

		unzip(zipFilePath, presentationFolderAbsolutePath);

		return presentationFolder;
	}

	private static void createOrCleanDirectory(File presentationFolder) {
		if (!presentationFolder.exists())
			presentationFolder.mkdirs();
		else
			cleanDirectory(presentationFolder);
	}

	public static boolean canWriteToSDCard() {
		boolean mExternalStorageAvailable = false;
		boolean mExternalStorageWriteable = false;
		String state = Environment.getExternalStorageState();

		if (Environment.MEDIA_MOUNTED.equals(state)) {
			mExternalStorageAvailable = mExternalStorageWriteable = true;
		}
		else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
			mExternalStorageAvailable = true;
			mExternalStorageWriteable = false;
		}
		else {
			mExternalStorageAvailable = mExternalStorageWriteable = false;
		}

		return mExternalStorageAvailable && mExternalStorageWriteable;
	}

	public static void zip(File[] filesToZip, File zipFile) {
		try {
			BufferedInputStream origin = null;
			FileOutputStream dest = new FileOutputStream(zipFile);

			ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(dest));

			byte data[] = new byte[BUFFER];

			for (int i = 0; i < filesToZip.length; i++) {
				FileInputStream fi = new FileInputStream(filesToZip[i]);
				origin = new BufferedInputStream(fi, BUFFER);
				String currentFileAbsolutePath = filesToZip[i].getAbsolutePath();
				String currentFileName = currentFileAbsolutePath.substring(currentFileAbsolutePath.lastIndexOf("/") + 1);
				ZipEntry entry = new ZipEntry(currentFileName);
				out.putNextEntry(entry);
				int count;
				while ((count = origin.read(data, 0, BUFFER)) != -1) {
					out.write(data, 0, count);
				}
				origin.close();
			}

			out.close();
		}
		catch (Exception e) {
			;
		}
	}

	public static void unzip(String zipFilePath, String unzippedFilesPath) {
		try {
			FileInputStream fin = new FileInputStream(zipFilePath);
			ZipInputStream zin = new ZipInputStream(fin);
			ZipEntry ze = null;
			while ((ze = zin.getNextEntry()) != null) {
				if (ze.isDirectory()) {
					createDirectoryIfRequired(unzippedFilesPath, ze.getName());
				}
				else {
					FileOutputStream fout = new FileOutputStream(unzippedFilesPath + ze.getName());
					for (int c = zin.read(); c != -1; c = zin.read()) {
						fout.write(c);
					}

					zin.closeEntry();
					fout.close();
				}
			}
			zin.close();
		}
		catch (Exception e) {
			;
		}
	}

	private static void createDirectoryIfRequired(String locationPath, String directoryName) {
		File f = new File(locationPath + directoryName);

		if (!f.exists() || !f.isDirectory()) {
			f.mkdirs();
		}
	}

	private static void cleanDirectory(File directory) {
		try {
			FileUtils.cleanDirectory(directory);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void copyfile(String srcFilePath, String destFilePath) {
		try {
			FileUtils.copyFile(new File(srcFilePath), new File(destFilePath));
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void deleteDirectory(File directory) {
		try {
			FileUtils.deleteDirectory(directory);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
