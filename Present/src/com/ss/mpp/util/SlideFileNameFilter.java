package com.ss.mpp.util;

import java.io.File;
import java.io.FilenameFilter;

public class SlideFileNameFilter implements FilenameFilter {
	public boolean accept(File parentDirectory, String fileName) {
		if (fileName.trim().startsWith("."))
			return false;
		return true;
	}
}
