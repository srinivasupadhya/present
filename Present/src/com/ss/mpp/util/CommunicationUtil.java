package com.ss.mpp.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.SocketException;
import java.net.URL;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public class CommunicationUtil {
	public static final int IO_BUFFER_SIZE = 8 * 1024;
	public static final String SERVER = "http://present-it.appspot.com/";
	public static final boolean CAN_COMMUNICATE = true;

	public static String startPresentation(String presentationName) {
		String urlString = SERVER + "StartPresentation?PRESENTATION_NAME=" + presentationName;

		String response = communicateToServer(urlString);

		if (!StringUtil.isBlank(response))
			return response.trim();

		return "";
	}

	public static boolean updatePresentationStatus(String meetingId, int position) {
		String urlString = SERVER + "UpdatePresentationStatus?MEETING_ID=" + meetingId + "&STATUS=" + position;

		String response = communicateToServer(urlString);

		if (!StringUtil.isBlank(response) && response.trim().equalsIgnoreCase("success"))
			return true;

		return false;
	}

	public static int getPresentationStatus(String meetingId) {
		String urlString = SERVER + "GetPresentationStatus?MEETING_ID=" + meetingId;

		String response = communicateToServer(urlString);

		if (!StringUtil.isBlank(response))
			return Integer.parseInt(response.trim());

		return 0;
	}

	private static String communicateToServer(String urlStr) {
		String line = null;
		if (CAN_COMMUNICATE) {
			HttpClient httpclient = new DefaultHttpClient();
			HttpGet httpget = new HttpGet(urlStr);
			try {
				HttpResponse response = httpclient.execute(httpget);
				if (response != null) {
					InputStream inputstream = response.getEntity().getContent();
					line = convertStreamToString(inputstream);
					// Toast.makeText(context, line, Toast.LENGTH_SHORT).show();
				}
				else {
					// Toast.makeText(context, "Unable to complete your request", Toast.LENGTH_LONG).show();
				}
			}
			catch (ClientProtocolException e) {
				// Toast.makeText(context, "Caught ClientProtocolException", Toast.LENGTH_SHORT).show();
			}
			catch (SocketException se) {
				// Toast.makeText(context, "Caught SocketException", Toast.LENGTH_SHORT).show();
			}
			catch (IOException e) {
				// Toast.makeText(context, "Caught IOException", Toast.LENGTH_SHORT).show();
			}
			catch (Exception e) {
				// Toast.makeText(context, "Caught Exception", Toast.LENGTH_SHORT).show();
			}
		}
		return line;
	}

	private static String convertStreamToString(InputStream is) {
		String line = "";
		StringBuilder total = new StringBuilder();
		BufferedReader rd = new BufferedReader(new InputStreamReader(is));
		try {
			while ((line = rd.readLine()) != null) {
				total.append(line);
			}
		}
		catch (Exception e) {
			// Toast.makeText(context, "Caught StreamException", Toast.LENGTH_SHORT).show();
		}
		return total.toString();
	}

	public static void uploadFile(String fileAbsolutePath) {
		HttpURLConnection connection = null;
		DataOutputStream outputStream = null;

		String urlServer = SERVER + "/UploadSlides";
		String lineEnd = "\r\n";
		String twoHyphens = "--";
		String boundary = "*****";

		int bytesRead, bytesAvailable, bufferSize;
		byte[] buffer;
		int maxBufferSize = 1 * 1024 * 1024;

		try {
			FileInputStream fileInputStream = new FileInputStream(new File(fileAbsolutePath));

			URL url = new URL(urlServer);
			connection = (HttpURLConnection) url.openConnection();

			// Allow Inputs & Outputs
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.setUseCaches(false);

			// Enable POST method
			connection.setRequestMethod("POST");

			connection.setRequestProperty("Connection", "Keep-Alive");
			connection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);

			outputStream = new DataOutputStream(connection.getOutputStream());
			outputStream.writeBytes(twoHyphens + boundary + lineEnd);
			outputStream.writeBytes("Content-Disposition: form-data; name=\"uploadedfile\";filename=\"" + fileAbsolutePath + "\"" + lineEnd);
			outputStream.writeBytes(lineEnd);

			bytesAvailable = fileInputStream.available();
			bufferSize = Math.min(bytesAvailable, maxBufferSize);
			buffer = new byte[bufferSize];

			// Read file
			bytesRead = fileInputStream.read(buffer, 0, bufferSize);

			while (bytesRead > 0) {
				outputStream.write(buffer, 0, bufferSize);
				bytesAvailable = fileInputStream.available();
				bufferSize = Math.min(bytesAvailable, maxBufferSize);
				bytesRead = fileInputStream.read(buffer, 0, bufferSize);
			}

			outputStream.writeBytes(lineEnd);
			outputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

			// Responses from the server (code and message)
			connection.getResponseCode();
			connection.getResponseMessage();

			fileInputStream.close();
			outputStream.flush();
			outputStream.close();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static File downloadFile(String meetingId, String fileAbsolutePath) {
		HttpURLConnection urlConnection = null;
		BufferedOutputStream out = null;
		// final File cacheFile = new File(cache.createFilePath(urlString));
		String urlString = SERVER + "/DownloadSlides?MEETING_ID=" + meetingId;
		final File outputFile = new File(fileAbsolutePath);

		try {
			final URL url = new URL(urlString);
			urlConnection = (HttpURLConnection) url.openConnection();
			final InputStream in = new BufferedInputStream(urlConnection.getInputStream(), IO_BUFFER_SIZE);
			out = new BufferedOutputStream(new FileOutputStream(outputFile), IO_BUFFER_SIZE);

			int b;
			while ((b = in.read()) != -1) {
				out.write(b);
			}

			return outputFile;
		}
		catch (final IOException e) {
			e.printStackTrace();
		}
		finally {
			if (urlConnection != null) {
				urlConnection.disconnect();
			}
			if (out != null) {
				try {
					out.close();
				}
				catch (final IOException e) {
					;
				}
			}
		}

		return null;
	}
}
