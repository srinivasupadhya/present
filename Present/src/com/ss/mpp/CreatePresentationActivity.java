package com.ss.mpp;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.ss.mpp.util.FileUtil;

public class CreatePresentationActivity extends Activity {
	String[] defaultImageFiles = { "/mnt/sdcard/DCIM/camera/a.jpg",
			"/mnt/sdcard/DCIM/camera/b.jpg", "/mnt/sdcard/DCIM/camera/c.jpg",
			"/mnt/sdcard/DCIM/camera/d.jpg", "/mnt/sdcard/DCIM/camera/e.jpg" };

	List<String> filePaths = new ArrayList<String>();

	ArrayAdapter<String> adapter;

	private static int RESULT_LOAD_IMAGE = 1;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_create_presentation);
		setTitle(R.string.title_activity_main);

		adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, filePaths);

		boolean imageExists = defaultImagesExists();

		if (imageExists) {
			filePaths.addAll(Arrays.asList(defaultImageFiles));
		}

		Button createPresentation = (Button) findViewById(R.id.add_photo);
		createPresentation.setOnClickListener(onAddPhoto);

		Button startPresentation = (Button) findViewById(R.id.create_presentation_done);
		startPresentation.setOnClickListener(onCreatePresentationDone);

		ListView list = (ListView) findViewById(R.id.selected_images);
		list.setAdapter(adapter);
	}

	private boolean defaultImagesExists() {
		for (String path : defaultImageFiles) {
			if (!new File(path).exists()) {
				return false;
			}
		}
		return true;
	}

	private View.OnClickListener onAddPhoto = new View.OnClickListener() {
		public void onClick(View v) {
			Intent i = new Intent(
					Intent.ACTION_PICK,
					android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
			startActivityForResult(i, RESULT_LOAD_IMAGE);
		}
	};

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK
				&& null != data) {
			String picturePath = getPicturePath(data);
			adapter.add(picturePath);
		}
	}

	private String getPicturePath(Intent data) {
		String[] filePathColumn = { MediaStore.Images.Media.DATA };
		Cursor cursor = getContentResolver().query(data.getData(),
				filePathColumn, null, null, null);
		cursor.moveToFirst();
		String picturePath = cursor.getString(cursor
				.getColumnIndex(filePathColumn[0]));
		cursor.close();
		return picturePath;
	}

	private View.OnClickListener onCreatePresentationDone = new View.OnClickListener() {
		public void onClick(View v) {
			new Thread() {
				public void run() {
					FileUtil.createPresentationDirectory(MPPContants.ZIP_NAME,
							filePaths);
					finish();
				}
			}.start();
		}
	};

}
