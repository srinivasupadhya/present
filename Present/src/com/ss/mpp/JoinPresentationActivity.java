package com.ss.mpp;

import java.io.File;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.Window;
import android.view.WindowManager;

import com.ss.mpp.uiwidget.CustomViewPager;
import com.ss.mpp.util.CollectionUtil;
import com.ss.mpp.util.CommunicationUtil;
import com.ss.mpp.util.Constants;
import com.ss.mpp.util.FileUtil;

public class JoinPresentationActivity extends FragmentActivity {
	private static final String CURRENT_MEETING_SLIDES = "CURRENT_MEETING_SLIDES";
	private static final String CURRENT_POSITION = "CURRENT_POSITION";
	private static final String MEETING_ID = "MEETING_ID";

	public static final String EXTRA_IMAGE = "extra_image";
	public static final String EXTRA_MEETING_ID = "extra_meeting_id";

	private String meetingId;
	private int currentPosition;

	private ImagePagerAdapter mAdapter;
	private CustomViewPager mPager;

	public static File[] currentMeetingSlides;

	private Handler h = new Handler();
	private Runnable animateViewPager = new Runnable() {
		public void run() {
			int position = CommunicationUtil.getPresentationStatus(meetingId);
			if (position != currentPosition) {
				mPager.setCurrentItem(position, true);
				currentPosition = position;
			}

			h.postDelayed(animateViewPager, Constants.POLL_INTERVAL);
		}
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_join_presentation);

		restoreFromSavedInstance(savedInstanceState);

		meetingId = MPPContants.MEETING_ID;

		if (CollectionUtil.isEmpty(currentMeetingSlides)) {
			currentMeetingSlides = FileUtil.getOtherPresentationSlides(meetingId);
			CollectionUtil.sortSlides(currentMeetingSlides);
			currentPosition = 0;
		}

		mAdapter = new ImagePagerAdapter(getSupportFragmentManager(), currentMeetingSlides.length);
		mPager = (CustomViewPager) findViewById(R.id.attendee_view);
		mPager.setAdapter(mAdapter);
		mPager.setPagingEnabled(false);

		h.postDelayed(animateViewPager, Constants.POLL_INTERVAL);
	}

	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		super.onSaveInstanceState(savedInstanceState);

		savedInstanceState.putString(MEETING_ID, meetingId);
		savedInstanceState.putInt(CURRENT_POSITION, currentPosition);
		savedInstanceState.putSerializable(CURRENT_MEETING_SLIDES, currentMeetingSlides);
	}

	private void restoreFromSavedInstance(Bundle savedInstanceState) {
		if (savedInstanceState != null) {
			meetingId = savedInstanceState.getString(MEETING_ID);
			currentPosition = savedInstanceState.getInt(CURRENT_POSITION);
			currentMeetingSlides = (File[]) savedInstanceState.getSerializable(CURRENT_MEETING_SLIDES);
		}
	}

	@Override
	public void onPause() {
		super.onPause();
		if (h != null) {
			h.removeCallbacks(animateViewPager);
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		h.postDelayed(animateViewPager, Constants.POLL_INTERVAL);
	}

	public static class ImagePagerAdapter extends FragmentStatePagerAdapter {
		private final int mSize;

		public ImagePagerAdapter(FragmentManager fm, int size) {
			super(fm);
			mSize = size;
		}

		@Override
		public int getCount() {
			return mSize;
		}

		@Override
		public Fragment getItem(int position) {
			return ImageDetailFragment.newInstance(position, currentMeetingSlides);
		}
	}
}