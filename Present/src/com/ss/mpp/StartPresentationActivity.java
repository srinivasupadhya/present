package com.ss.mpp;

import java.io.File;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.ss.mpp.util.CollectionUtil;
import com.ss.mpp.util.CommunicationUtil;
import com.ss.mpp.util.Constants;
import com.ss.mpp.util.FileUtil;
import com.ss.mpp.util.StringUtil;

public class StartPresentationActivity extends FragmentActivity {
	private static final String CURRENT_MEETING_SLIDES = "CURRENT_MEETING_SLIDES";
	private static final String CURRENT_POSITION = "CURRENT_POSITION";
	private static final String PREVIOUS_POSITION = "PREVIOUS_POSITION";
	private static final String MEETING_ID = "MEETING_ID";
	private static final String CURRENT_PRESENTATION_NAME = "CURRENT_PRESENTATION_NAME";

	public static final String EXTRA_IMAGE = "extra_image";
	public static final String EXTRA_PRESENTATION_NAME = "extra_presentation_name";

	private String presentationName = MPPContants.ZIP_NAME;

	private String meetingId;

	private int currentPosition;
	private int previousPosition;

	private ImagePagerAdapter mAdapter;
	private ViewPager mPager;

	public static File[] currentMeetingSlides;

	private Handler h = new Handler();
	private Runnable serverUpdater = new Runnable() {
		public void run() {
			if (previousPosition != currentPosition) {
				CommunicationUtil.updatePresentationStatus(meetingId,
						currentPosition);
				previousPosition = currentPosition;
			}

			h.postDelayed(serverUpdater, Constants.POLL_INTERVAL);
		}
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle(R.string.title_activity_main);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_start_presentation);

		restoreFromSavedInstance(savedInstanceState);

		if (CollectionUtil.isEmpty(currentMeetingSlides)) {
			currentMeetingSlides = FileUtil
					.getUserPresentationSlides(presentationName);
			CollectionUtil.sortSlides(currentMeetingSlides);
		}

		if (StringUtil.isBlank(meetingId)) {
			meetingId = MPPContants.MEETING_ID;
			previousPosition = currentPosition = 0;
			CommunicationUtil.startPresentation(MPPContants.ZIP_NAME);
			Toast.makeText(this, meetingId, Toast.LENGTH_SHORT).show();
		}

		mAdapter = new ImagePagerAdapter(getSupportFragmentManager(),
				currentMeetingSlides.length);
		mPager = (ViewPager) findViewById(R.id.presenter_view);
		mPager.setAdapter(mAdapter);
		mPager.setOnPageChangeListener(viewPager);

		h.postDelayed(serverUpdater, Constants.POLL_INTERVAL);
	}

	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		super.onSaveInstanceState(savedInstanceState);

		savedInstanceState.putString(CURRENT_PRESENTATION_NAME,
				presentationName);
		savedInstanceState.putString(MEETING_ID, meetingId);
		savedInstanceState.putInt(PREVIOUS_POSITION, previousPosition);
		savedInstanceState.putInt(CURRENT_POSITION, currentPosition);
		savedInstanceState.putSerializable(CURRENT_MEETING_SLIDES,
				currentMeetingSlides);
	}

	private void restoreFromSavedInstance(Bundle savedInstanceState) {
		if (savedInstanceState != null) {
			presentationName = savedInstanceState
					.getString(CURRENT_PRESENTATION_NAME);
			meetingId = savedInstanceState.getString(MEETING_ID);
			previousPosition = savedInstanceState.getInt(PREVIOUS_POSITION);
			currentPosition = savedInstanceState.getInt(CURRENT_POSITION);
			currentMeetingSlides = (File[]) savedInstanceState
					.getSerializable(CURRENT_MEETING_SLIDES);
		}
	}

	private ViewPager.OnPageChangeListener viewPager = new ViewPager.OnPageChangeListener() {
		public void onPageSelected(int position) {
			currentPosition = position;
		}

		public void onPageScrolled(int arg0, float arg1, int arg2) {
		}

		public void onPageScrollStateChanged(int arg0) {
		}
	};

	@Override
	public void onPause() {
		super.onPause();
		if (h != null) {
			h.removeCallbacks(serverUpdater);
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		h.postDelayed(serverUpdater, Constants.POLL_INTERVAL);
	}

	public static class ImagePagerAdapter extends FragmentStatePagerAdapter {
		private final int mSize;

		public ImagePagerAdapter(FragmentManager fm, int size) {
			super(fm);
			mSize = size;
		}

		@Override
		public int getCount() {
			return mSize;
		}

		@Override
		public Fragment getItem(int position) {
			return ImageDetailFragment.newInstance(position,
					currentMeetingSlides);
		}
	}
}