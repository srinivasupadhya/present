package com.ss.mpp;

import java.io.File;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

public class ImageDetailFragment extends Fragment {
	private static final String IMAGE_DATA_EXTRA = "resId";
	private static final String SLIDES_EXTRA = "slides";

	private int mImageNum;
	private ImageView mImageView;
	private File[] slides;

	static ImageDetailFragment newInstance(int imageNum, File[] slides) {
		final ImageDetailFragment f = new ImageDetailFragment();
		final Bundle args = new Bundle();
		args.putInt(IMAGE_DATA_EXTRA, imageNum);
		args.putSerializable(SLIDES_EXTRA, slides);
		f.setArguments(args);
		return f;
	}

	// Empty constructor, required as per Fragment docs
	public ImageDetailFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mImageNum = getArguments() != null ? getArguments().getInt(IMAGE_DATA_EXTRA) : -1;
		slides = (File[]) (getArguments() != null ? getArguments().getSerializable(SLIDES_EXTRA) : null);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// image_detail_fragment.xml contains just an ImageView
		final View v = inflater.inflate(R.layout.image_detail_fragment, container, false);
		mImageView = (ImageView) v.findViewById(R.id.imageView);
		return v;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		final File slide = slides[mImageNum];
		Bitmap bitmap = BitmapFactory.decodeFile(slide.getAbsolutePath());
		mImageView.setImageBitmap(bitmap); // Load image into ImageView
	}
}
