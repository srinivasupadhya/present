<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Presentation -</title>
<script src="http://yui.yahooapis.com/3.6.0/build/yui/yui-min.js"></script>
<script type="text/javascript">
	YUI({
		//Last Gallery Build of this module
		gallery : 'gallery-2011.04.20-13-04'
	}).use('gallery-yui-slideshow', function(Y) {
		// Slideshow 1:
		var slideshow1 = new Y.Slideshow({
			srcNode : '#slideshow1',
			transition : Y.Slideshow.PRESETS.fade,
			nextButton : '#nextButton',
			previousButton : '#prevButton',
			autoplay : false
		});

		slideshow1.render();
	});
	
	var currentSlideNumber = 0;
	
	YUI().use('datasource-io', 'datasource-polling', 'json-parse', function(Y) {
	    //var onlineFriends = Y.one('#friend-count'),
	    var intervalId;
	        
	    friendData = new Y.DataSource.IO({
	        source: 'GetPresentationStatus?MEETING_ID=Yahoo_Hack_2012'
	    });

	    // Start polling the server every 10 seconds
	    intervalId = friendData.setInterval(1000, {
	        request : '',
	        callback: {
	            success: function (e) {
	                //var friends = Y.JSON.parse(e.response.results[0]).friendCount;
	                var newSlideNumber = e.response.results[0].responseText;

	                if (!newSlideNumber) {
	                    //friends = 'No friends. You should go outside more.';
	                    newSlideNumber = 0;
	                }
	                
	                if(currentSlideNumber != newSlideNumber) {
	                	var difference = newSlideNumber - currentSlideNumber;
	                	if(difference > 0) {
	                		for(i=1; i <= difference; i++)
	                			document.getElementById("nextButton").click();
	                	}
	                	else if(difference < 0) {
	                		difference = -difference;
	                		for(i=1; i <= difference; i++)
	                			document.getElementById("prevButton").click();
	                	}
	                	
	                	currentSlideNumber = newSlideNumber;
	                }

	                //console.log(e);
	                //onlineFriends.set('text', friends);
	            },
	            failure: function (e) {
	                //onlineFriends.set('text', '(Bang) Ouch! ' + e.error.message + ' happened!');

	                // Stop polling
	                friendData.clearInterval(intervalId);
	            }
	        }
	    });
	});
</script>
<link rel="stylesheet" href="css/join_presentation.css" type="text/css">
</head>
<body>
	<p style="visibility: hidden;">
		<a id="prevButton" href="javascript:void(0);">Previous Slide</a><br />
		<a id="nextButton" href="javascript:void(0);">Next Slide</a><br />
	</p>
	
	<input id="meeting-id" type="hidden" value="" />
	<div id="friend-count"></div>

	<div id="slideshow1" class="slideshow">
		<%  Integer numberOfSlides = (Integer) request.getAttribute("NUMBER_OF_SLIDES");
			for(int i=1; i <= numberOfSlides; i++) { %>
				<img src="DownloadSingleSlide?SLIDE_NUMBER=<%= i %>" />
		<%  } %>
	</div>

</body>
</html>