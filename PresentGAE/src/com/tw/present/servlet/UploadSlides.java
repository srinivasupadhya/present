package com.tw.present.servlet;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;

import javax.jdo.PersistenceManager;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.IOUtils;

import com.google.appengine.api.datastore.Blob;
import com.tw.present.constant.Constants;
import com.tw.present.model.PMF;
import com.tw.present.model.Presentation;

public class UploadSlides extends HttpServlet {
	private static final long serialVersionUID = 1L;

	// Your upload handle would look like
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			// Get the image representation
			ServletFileUpload upload = new ServletFileUpload();

			FileItemIterator iterator = upload.getItemIterator(request);
			FileItemStream zipFileItemStream = iterator.next();
			InputStream zipInputStream = zipFileItemStream.openStream();

			// construct our entity objects
			Blob zipBlob = new Blob(IOUtils.toByteArray(zipInputStream));
			String zipFilePath = zipFileItemStream.getName();
			String zipFileName = zipFilePath.substring(zipFilePath.lastIndexOf("/") + 1);
			String presentationName = zipFileName.substring(0, zipFileName.lastIndexOf("."));
			Presentation presentation = new Presentation(presentationName, zipBlob);

			// persist image
			PersistenceManager pm = PMF.get().getPersistenceManager();
			pm.makePersistent(presentation);
			pm.close();

			// respond to query
			response.setContentType(Constants.TEXT_PLAIN);
			PrintWriter out = response.getWriter();
			out.print(Constants.SUCCESS);
			out.close();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
