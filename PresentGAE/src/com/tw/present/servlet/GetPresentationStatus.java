package com.tw.present.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.tw.present.constant.Constants;

public class GetPresentationStatus extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// String meetingId = request.getParameter(Constants.MEETING_ID);
		String meetingId = Constants.YAHOO_HACK_2012;

		Map<String, String> presentationStatusMap = (Map<String, String>) getServletContext().getAttribute(Constants.PRESENTATION_STATUS);
		if (presentationStatusMap == null)
			presentationStatusMap = new HashMap<String, String>();
		String status = presentationStatusMap.get(meetingId);

		response.setContentType(Constants.TEXT_PLAIN);
		PrintWriter out = response.getWriter();
		out.print(status);
		out.close();
	}
}
