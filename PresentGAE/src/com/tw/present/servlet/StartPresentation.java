package com.tw.present.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.tw.present.constant.Constants;

public class StartPresentation extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// String presentationName = request.getParameter(Constants.PRESENTATION_NAME);
		String presentationName = Constants.YAHOO_HACK_2012;
		// String meetingId = RandomStringUtils.randomNumeric(4);
		String meetingId = Constants.YAHOO_HACK_2012;

		Map<String, String> meetingToPresentationMap = (Map<String, String>) getServletContext().getAttribute(Constants.MEETING_TO_PRESENTATION_MAP);
		if (meetingToPresentationMap == null)
			meetingToPresentationMap = new HashMap<String, String>();
		meetingToPresentationMap.put(meetingId, presentationName);
		getServletContext().setAttribute(Constants.MEETING_TO_PRESENTATION_MAP, meetingToPresentationMap);

		Map<String, String> presentationStatusMap = (Map<String, String>) getServletContext().getAttribute(Constants.PRESENTATION_STATUS);
		if (presentationStatusMap == null)
			presentationStatusMap = new HashMap<String, String>();
		presentationStatusMap.put(meetingId, "0");
		getServletContext().setAttribute(Constants.PRESENTATION_STATUS, presentationStatusMap);

		response.setContentType(Constants.TEXT_PLAIN);
		PrintWriter out = response.getWriter();
		out.print(meetingId);
		out.close();
	}
}
