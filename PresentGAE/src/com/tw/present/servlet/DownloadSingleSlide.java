package com.tw.present.servlet;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;

import com.google.appengine.api.datastore.Blob;
import com.tw.present.constant.Constants;
import com.tw.present.model.PMF;
import com.tw.present.model.Presentation;

public class DownloadSingleSlide extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// String meetingId = request.getParameter(Constants.MEETING_ID);
		String meetingId = Constants.YAHOO_HACK_2012;

		String slideNumber = request.getParameter("SLIDE_NUMBER");

		Map<String, String> meetingToPresentationMap = (Map<String, String>) getServletContext().getAttribute(Constants.MEETING_TO_PRESENTATION_MAP);
		if (meetingToPresentationMap == null)
			meetingToPresentationMap = new HashMap<String, String>();
		String presentationName = meetingToPresentationMap.get(meetingId);
		getServletContext().setAttribute(Constants.MEETING_TO_PRESENTATION_MAP, meetingToPresentationMap);

		// find desired image
		PersistenceManager pm = PMF.get().getPersistenceManager();

		Query query = pm.newQuery(Presentation.class);
		query.setFilter("name == presentationNameParam");
		query.setOrdering("create desc");
		query.declareParameters("String presentationNameParam");

		List<Presentation> results = (List<Presentation>) query.execute(presentationName);
		Blob slidesZip = results.iterator().next().getZip();

		ByteArrayInputStream bais = new ByteArrayInputStream(slidesZip.getBytes());
		JarInputStream jis = new JarInputStream(bais);

		String fileName = null, fileExtension = null;
		byte[] bytes = null;
		JarEntry entry;
		while ((entry = jis.getNextJarEntry()) != null) {
			String fileFullName = entry.getName();
			fileName = fileFullName.substring(0, fileFullName.lastIndexOf("."));
			fileExtension = fileFullName.substring(fileFullName.lastIndexOf(".") + 1);

			if (fileName.equals(slideNumber)) {
				bytes = IOUtils.toByteArray(jis);
			}
		}

		jis.close();

		// serve the first image
		response.setContentType("application/" + fileExtension);
		response.setHeader("content-disposition", "inline; filename=\"" + fileName + "." + fileExtension + "\"");
		response.getOutputStream().write(bytes);
	}
}
