package com.tw.present.constant;

public class Constants {
	public static final String MEETING_TO_PRESENTATION_MAP = "MEETING_TO_PRESENTATION_MAP";
	public static final String PRESENTATION_STATUS = "PRESENTATION_STATUS";
	
	public static final String PRESENTATION_NAME = "PRESENTATION_NAME";
	public static final String MEETING_ID = "MEETING_ID";
	public static final String STATUS = "STATUS";
	
	public static final String YAHOO_HACK_2012 = "Yahoo_Hack_2012";
	
	public static final String TEXT_PLAIN = "text/plain";
	
	public static final String SUCCESS = "success";
}
