## Initial Setup:
Download Eclipse
Install plugins:
Android: https://dl-ssl.google.com/android/eclipse
  Maven: http://download.eclipse.org/technology/m2e/releases
    Git: http://download.eclipse.org/egit/updates

## Present:
New Project -> Android project from existing source
Run as Android application

## PresentOnline:
Import PresentOnline as 'Existing Maven Projects'
cd to PresentOnline directory
Run commands:
	1. mvn clean install
	2. mvn jetty:run
Server running at localhost:8080

Team:
1. Srini
2. Sriki
